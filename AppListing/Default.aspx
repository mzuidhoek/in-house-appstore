﻿<%@ Page Title="Apps" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="AppListing._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h1><%= CurrentPath %></h1>
    <div>
        <table class="contentList">
            <tr><th>Build naam:</th><th>Aangemaakt op:</th></tr>
            <% foreach(string item in GetCurrentFolderContents()){ %>
                <tr><%= item %></tr>
            <% } %>
        </table>
    </div>

</asp:Content>
