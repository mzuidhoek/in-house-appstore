﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;

namespace AppListing
{
    public partial class _Default : Page
    {
        private string _currentPath;
        public string CurrentPath { get { return string.Format("Apps/{0}", _currentPath); } }

        private string _root = HttpContext.Current.Server.MapPath("~/Apps/");

        protected void Page_Load(object sender, EventArgs e)
        {
            _currentPath = Request.QueryString["path"];
        }

        public List<string> GetCurrentFolderContents()
        {
            List<string> contents = new List<string>();
            

            string path = string.Format("{0}{1}", _root, _currentPath);

            foreach (string item in Directory.GetDirectories(path))
            {
                string name = Path.GetFileName(item);
                    contents.Add(string.Format("<td><a href='?path={0}{1}/'>{1}</a></td><td />", _currentPath, name));
            }

            contents.AddRange(GetCurrentFolderFiles(path));
            return contents;
        }

        public List<string> GetCurrentFolderFiles(string path)
        {
            List<string> filenames = new List<string>();

            DirectoryInfo di = new DirectoryInfo(path);
            FileSystemInfo[] files = di.GetFileSystemInfos();
            var orderedFiles = files.Where(f => !Directory.Exists(f.FullName)).OrderByDescending(f => f.CreationTime).ToList();

            foreach(var file in orderedFiles)
            {
                var rootUri = new Uri(_root);
                var websitePath = rootUri.MakeRelativeUri(new Uri(file.FullName)).ToString();
                filenames.Add(string.Format("<td><a href='Apps/{0}'>{1}</a></td><td>{2}</td>", websitePath, file.Name, file.CreationTime.ToString("dd-MM-yyyy hh:mm:ss")));
            }

            return filenames;
        }
    }
}